<?php
	$type        = 'Data Visualization'; 
	$date        = date("F d, Y");
	$title       = 'Tax burdens and city-suburb tax gap for families in Philadelphia'; 
	$subtitle    = 'Subtitle empty'; 
	$description = 'Description not provided';
	$related_project = 'Related project not provided';
	$interactivePosition= 1; //position = 1 : full-width | position = 2 : column-width