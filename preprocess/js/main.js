// @codekit-prepend "variables.js"
var vizfile = "/~/media/data-visualizations/interactives/2016/taxburdens/assets/data.tsv.txt"
var mapfile = "/~/media/data-visualizations/interactives/2016/taxburdens/assets/map.small.json"
function parseDelimited(data,delimiter){
    // Split the lines
    var lines = data.split(/\r\n|\n|\r/);
    var propList=[];
    var returnarray=[];
    $.each(lines, function(lineNumber, line) {
        var items = line.split(delimiter);
        // properties from first line
        if (lineNumber == 0) {
            propList = items;
        }
        else {
            var foo = {};
            $.each(items, function(index, item) {
                foo[propList[index]] = isNaN(parseFloat(item))?item:parseFloat(item);
            });
            returnarray.push(foo);
        }
    });
    return returnarray;
}


$(document).ready(function() {
    $.when(
        $.get(vizfile),
        $.getJSON(mapfile)
    )
    .done(function(v,m) {
        var dataJSON = parseDelimited(v[0],'\t');
        var mapdata = m[0];
        var db = TAFFY(dataJSON);
    queryObject = {
        "Year"              : 2015,
        "Income level"      : "Median",
        "Household Type"    : "Resident",
        "Jurisdiction level": "Town"
    }

        var vizdata=db(queryObject).each(function(r){
            r.value=r["Tax burden gap"];
            r.name=r["Town"];
            r.code=r["FIPS"]
        }).get();


        $('#container').highcharts('Map', {

            chart: {
                backgroundColor: null
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Philly Tax Burden'
            },
            mapNavigation: {
                enabled: true,
                enableDoubleClickZoomTo: true,
                buttonOptions: {
                    verticalAlign: 'bottom'
                }
            },
            colorAxis: {
                stops: [
                    [0, '#3060cf'],
                    [0.5, '#fffbbc'],
                    [1, '#c4463a']
                ],
                min: -4000,
                max:4000
            },

            series: [{
                data: vizdata,
                mapData: mapdata,
                joinBy: ['GEOID','FIPS' ],
                name: 'Philly Tax Burden',
                borderColor: 'black',
                borderWidth: 0.2,
                allowPointSelect: true,
                states: {
                    hover: {
                        borderWidth: 1
                    }
                }
            }],
            tooltip: {
                formatter: function () {
                    return '<b>Series name: ' + this.series.name + '</b><br>' +
                    'Point name: ' + this.point.Town + '<br>' +
                    'Value: ' + this.point.value;
                }
            }
        });
    });
});
    // queryFields=[Town,County,State,Year,Income,Type,Jlevel]
    //     Town   :"",
    //     County :"",
    //     State  :"",
    

        //  "Town"                :  Town   ,
        //  "County"              :  County ,
        //  "State"               :  State  ,
        //  "Tax burden Pct"      :  9.06,
        //  "Tax burden Pct gap"  :  -4.48,
        //  "Tax burden"          :  5932.68,
        //  "Tax burden gap"      :  -2934.91

//var vizdata = db({Town:"Bass River Township"}).select("County","Tax burden Pct")

      
    //console.log(vizdata.count())
      
    //console.log(vizdata)
    // $.getJSON(datafile, function (data) {
    // });

    // Initiate the chart





    //var options = {
    //     chart: {
    //         renderTo: 'container',
    //         type: 'column'
    //     },
    //     title: {
    //         text: 'Test'
    //     },
    //     xAxis: {
    //         categories: []
    //     },
    //     yAxis: {
    //         title: {
    //             text: 'Units'
    //         }
    //     },
    //     credits: {
    //         enabled: false
    //     },
    //     series: []
    // };



