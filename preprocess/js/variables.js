

    var twitterLink = "bit.ly/askljdfh/";
    var chart_figure_data,
        segmentCode_curr,
        segmentsinfo,
        chartTitles,
        plottedchart,
        imgsrc = '/~/media/data-visualizations/interactives/2016/chart001/assets/img/',
        tab = "benefits",
        fontstackWhitney = '"Whitney SSm A", "Whitney SSm B", "Helvetica Neue", "HelveticaNeue", Arial, Helvetica, "Roboto", sans-serif';
        fontstackMuseo   = '"MuseoSlab 500",MuseoSlab_500,MuseoSlab500';

        red         ='#EF676A',
        reddark     ='#C93638',
        aqua        ='#5CBFBF',
        aquadark    ='#1C939B',
        blue        ='#064950',
        bluedark    ='#042626',
        bluecolumn  ='#095B62',
        linegrey    ='#CCCCCC',
        textgray    ='#98999a',
        textdarkgrey='#616161',
        textlight   ='#F9F9F9';
        slate       ='#565656';
        colors={tooltip:slate};
    var AxisFontSize = '12px'

    var percentileColumn = [];


    var chart_options = {
        tooltip: {
            valuePrefix: '$'
        },
        chart: {
            animation: false,
            backgroundColor: null
        },
        credits: {
            enabled: false
        },
        legend: {
            itemStyle: {
                //font: '9pt "Tungsten",Trebuchet MS, Verdana, sans-serif',
                color: 'black'
            },
            itemHoverStyle: {
                color: 'gray'
            },
            enabled: false
        }
    };


    var tooltip = {
            tooltip: {
                enabled: true,
                shape: 'callout',
                valueSuffix: null,
                valuePrefix: '',
                backgroundColor: colors.tooltip, //tab_color[tab],  
                borderColor: colors.tooltip, //null,
                crosshairs: null,
                borderRadius: 8,
                borderWidth: 2,
                useHTML: true,
                style: {
                    color: '#ffffff',
                    fontFamily: '"MuseoSlab 300"',
                    fontSize: '11px'
                },
                animation: true,
                hideDelay: 500,
                shadow: false,
                shared: false,
                followPointer: false,
                followTouchMove: false

            }
        } //tooltip_theme
